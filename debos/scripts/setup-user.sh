#!/bin/bash

set -e

echo "I: Creating user"

if ! getent passwd user > /dev/null ; then
  adduser --gecos "user" --quiet --disabled-login user
  echo "user:user" | chpasswd
fi

# Force the addition of the sudo group here, even if sudo isn't installed such
# that if it gets installed the user user is part of it
addgroup --quiet --system sudo || true
adduser user sudo
